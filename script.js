const showPasswordIcons = document.querySelectorAll('.show-password');
const hidePasswordIcons = document.querySelectorAll('.hide-password');
const passwordInputs = document.querySelectorAll('input[type="password"]');
const btn = document.querySelector('.btn');

showPasswordIcons.forEach((icon, index) => {
    icon.addEventListener('click', () => {
        passwordInputs[index].type = 'text';
        showPasswordIcons[index].style.display = 'none';
        hidePasswordIcons[index].style.display = 'block';
    });
});

hidePasswordIcons.forEach((icon, index) => {
    icon.addEventListener('click', () => {
        passwordInputs[index].type = 'password';
        showPasswordIcons[index].style.display = 'block';
        hidePasswordIcons[index].style.display = 'none';
    });
});

btn.addEventListener('click', (e) => {
    e.preventDefault();
    const password1 = passwordInputs[0].value;
    const password2 = passwordInputs[1].value;

    if (password1 === password2 && password1 !== '' && password2 !== '') {
        alert('You are welcome');
    } else {
        const additionalElement = document.createElement('p');
        additionalElement.textContent = 'Потрібно ввести однакові значення і щоб поля не були пусті';
        additionalElement.classList.add('writing');
        additionalElement.style.cssText = "color: red; margin-top: 0;";
        const submitButton = document.querySelector('.btn');
        submitButton.parentNode.insertBefore(additionalElement, submitButton);
    }
});
